class AdvertiseElements(object):
    click_on_advertise = '.menu.primary-menu > a:nth-of-type(1)'
    click_on_register = '.lead-banner .button:nth-child(3)'
    explore_features = '.lead-secondary'
    enter_email = 'input#EMAIL'
    enter_first_name ='#FIRSTNAME'
    enter_last_name ='input#LASTNAME'
    agree_to_newsletters = '.checkbox.checkbox_tick_positive'
    click_subscribe = '.sib-form-block__button.sib-form-block__button-with-loader'