from test.advertise_with_us.advertise_page_elements import AdvertiseElements
import dotenv

dotenv.load_dotenv()


class AdvertisePageObjects(AdvertiseElements):

    def open_advertise_with_us(self):
        self.open(self.url)
        self.click(self.click_on_advertise)

    def register(self):
        self.click(self.click_on_register)

    def enter_details(self, name, email):
        self.update_text(self.enter_first_name, name)
        self.update_text(self.enter_email, email)
        self.click(self.enter_last_name)
        self.click(self.agree_to_newsletters)

    def subscribe(self):
        self.click(self.click_subscribe)
