from test.advertise_with_us.advertise_page_objects import AdvertisePageObjects
from test.base_test_case import BaseTestCase
import dotenv
import os

dotenv.load_dotenv()


class AdvertiseWithUs(BaseTestCase, AdvertisePageObjects):
    def test_advertise_page(self):
        name = os.getenv('NAME')
        email = os.getenv('EMAIL')

        self.open_advertise_with_us()
        self.register()
        self.enter_details(name, email)
        self.subscribe()
