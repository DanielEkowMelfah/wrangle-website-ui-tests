from seleniumbase import BaseCase
import dotenv
import os

dotenv.load_dotenv()


class BaseTestCase(BaseCase):
    url = os.getenv('URL')

    def setUp(self):
        super(BaseTestCase, self).setUp()
        # <<< Add custom setUp code for tests AFTER the super().setUp() >>>

    def tearDown(self):
        self.save_teardown_screenshot()

        super(BaseTestCase, self).tearDown()



