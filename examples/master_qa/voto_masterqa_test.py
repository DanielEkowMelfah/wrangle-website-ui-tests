from seleniumbase import MasterQA


class VotoMasterQATests(MasterQA):

    def test_voto(self):
        self.open("https://viamo.io/")

        self.verify("Confrim you're on the index page?")
        self.open("https://viamo.io/careers/")
        self.verify("Can you find 'careers with viamo'?")

        self.open("https://viamo.io/vacancies/")

        self.verify("Can you find any software engineer role?")
