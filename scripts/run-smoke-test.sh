#!/bin/bash

echo "Smoke test beginning now...."

echo "Create a new organisation test starting...."
pytest -v test/smoke/user_access/add_new_organisation_test.py

echo "Now call test starting...."
pytest -v test/smoke/outbound_calls/send_now_call_test.py

echo "Fixed date call test starting...."
pytest -v test/smoke/outbound_calls/send_fixed_date_call_test.py

echo "Routine call test starting...."
pytest -v test/smoke/outbound_calls/send_routine_call_test.py

echo "Repeating call test starting...."
pytest -v test/smoke/outbound_calls/send_repeating_call_test.py


