#!/bin/bash

echo "Smoke test beginning now...."

echo "Create a new organisation test starting...."
pytest -v test/smoke/user_access/add_new_organisation_test.py --headless

echo "Creating a new contact test starting..."
pytest -v test/smoke/subscribers/subscribers_test.py --headless

echo "Switching to existing org test starting..."
pytest -v test/smoke/user_access/switch_to_org_test.py --headless

echo "Starting a Call to Record test..."
pytest -v test/smoke/content_syncing/ctr_test.py --headless

echo "Now call test starting...."
pytest -v test/smoke/outbound_calls/send_now_call_test.py --headless

echo "Fixed date call test starting...."
pytest -v test/smoke/outbound_calls/send_fixed_date_call_test.py --headless

echo "Routine call test starting...."
pytest -v test/smoke/outbound_calls/send_routine_call_test.py --headless

echo "Repeating call test starting...."
pytest -v test/smoke/outbound_calls/send_repeating_call_test.py --headless


