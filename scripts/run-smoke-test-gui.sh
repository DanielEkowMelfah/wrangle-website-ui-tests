#!/bin/bash

echo "Smoke test beginning now...."

echo "Create a new organisation test starting...."
pytest -v test/smoke/user_access/add_new_organisation_test.py --gui --browser=chrome --demo --html=report.html

echo "Creating a new contact test starting..."
pytest -v test/smoke/subscribers/subscribers_test.py --gui --demo  --browser=chrome --reuse-session --html=report.html

echo "Starting a Call to Record test..."
pytest -v test/smoke/content_syncing/ctr_test.py --gui --demo  --browser=chrome --reuse-session --html=report.html

echo "Switching to existing org test starting..."
pytest -v test/smoke/user_access/switch_to_org_test.py --gui --demo  --browser=chrome --html=report.html

echo "Now call test starting...."
pytest -v test/smoke/outbound_calls/send_now_call_test.py --gui --demo  --browser=chrome --html=report.html

echo "Fixed date call test starting...."
pytest -v test/smoke/outbound_calls/send_fixed_date_call_test.py --gui --demo  --browser=chrome --html=report.html

echo "Routine call test starting...."
pytest -v test/smoke/outbound_calls/send_routine_call_test.py --gui --demo  --browser=chrome --html=report.html

echo "Repeating call test starting...."
pytest -v test/smoke/outbound_calls/send_repeating_call_test.py --gui --demo  --browser=chrome --html=report.html


